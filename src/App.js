import './App.css';
import Button from './components/Button';
import ButtonBox from './components/ButtonBox';
import Wrapper from './components/Wrapper';
import Screen from './components/Screen';

import React, { useState } from 'react';

const btnValues = [
  ["C", "1/x", "\u221a", "/"],
  [7, 8, 9, "X"],
  [4, 5, 6, "-"],
  [1, 2, 3, "+"],
  [0, ".", "="]
];


function App() {

  let [calc, setCalc] = useState({
    a: "0",
    r: 0,
    op: "",
    s: 0
  });

  function evaluator(event) {
    event.preventDefault();
    const value = event.target.innerHTML;
    if (value === "C") {
      setCalc({ a: "0", r: 0, op: "", s: 0 });
    } else if ("0123456789.".includes(value)) {
      let new_acc = acumulator_editor(value);
      if (calc.s === 0 || calc.s === 1) {
        setCalc({ ...calc, a: new_acc });
      } else if (calc.s === 2) {
        setCalc({ ...calc, a: new_acc, s: 0 });
      } else if (calc.s === 3) {
        setCalc({ ...calc, a: new_acc, s: 1 });
      } else if (calc.s === 4) {
        setCalc({ ...calc, a: value === '.' ? '0.' : value, s: 0 });
      }

    } else if (value === "=") {
      if ([1, 4].includes(calc.s)) {
        let new_r = make_operation(calc.r, calc.a, calc.op);
        if (new_r) {
          setCalc({ ...calc, r: new_r, s: 4 });
        }

      }
    } else if ("+-X/".includes(value)) {
      if (calc.s === 0) {
        setCalc({ r: parseFloat(calc.a), a: "0", op: value, s: 3 });
      } else if (calc.s === 1) {
        let new_r = make_operation(calc.r, calc.a, calc.op);
        if (new_r) {
          setCalc({ a: "0", r: new_r, op: value, s: 3 });
        }
      } else {
        setCalc({ ...calc, op: value, s: 3, a: "0" });
      }
    } else if (value === "1/x") {
      if (calc.s < 2) {
        let acc = parseFloat(calc.a);
        if (Math.abs(acc) > 1e-12)
          setCalc({ r: 1 / acc, a: "0", op: "", s: 2 });
      } else {
        if (Math.abs(calc.r) > 1e-12)
          setCalc({ r: 1 / calc.r, a: "0", op: "", s: 2 });
      }
    } else if (value === "\u221a") {
      if (calc.s < 2) {
        let acc = parseFloat(calc.a);
        if (acc > 0) {
          setCalc({ r: Math.sqrt(acc), a: "0", op: "", s: 2 });
        }
      } else {
        if (calc.r > 0) {
          setCalc({ r: Math.sqrt(calc.r), a: "0", op: "", s: 2 });
        }
      }
    }
  }

  function acumulator_editor(btn) {
    // if (calc.a === "0" && btn === "0") {
    //   return "0";
    // }
    if (calc.a === "0" && "123456789".includes(btn)) {
      return btn;
      // setCalc({...calc, a: btn});
    } else if (btn === "." && !calc.a.includes(".")) {
      return calc.a + ".";
      // setCalc({...calc, a: calc.a + "."})
    } else if (calc.a !== "0" && btn !== ".") {
      return calc.a + btn;
      // setCalc({...calc, a: calc.a + btn});
    }
    return "0";
  }

  function make_operation(prev_r, a, op) {
    let acc = parseFloat(a);
    if (op === "+") {
      return prev_r + acc;
    }
    if (op === "-") {
      return prev_r - acc;
    }
    if (op === "X") {
      return prev_r * acc;
    }
    if (op === "/" && Math.abs(acc) > 1e-10) {
      return prev_r / acc;
    }
    return undefined;
  }



  // function evaluator(event) {
  //   console.log(calc);
  //   event.preventDefault();
  //   const value = event.target.innerHTML;

  //   if (value === "C") {
  //     setCalc({
  //       ...calc,
  //       op1: 0,
  //       op2: 0,
  //       res: 0,
  //       sign: ""
  //     });
  //     console.log('wyzerowano');
  //     return;
  //   }

  //   if (value === "+-") {
  //     // console.log(value);
  //     let tmp = calc.op1;
  //     setCalc({
  //       ...calc,
  //       op1: -tmp,
  //       op2: 0,
  //       res: 0,
  //       sign: ""
  //     });
  //     console.log('zmieniono znak');
  //     return;
  //   }

  //   if (value === "+") {
  //     setCalc({
  //       ...calc,
  //       sign: "+"
  //     });
  //   }

  //   // console.log(value);
  //   const val2 = parseInt(value);

  //   if (Number.isInteger(val2)) {
  //     if (calc.op1 === 0) {
  //       setCalc({
  //         ...calc,
  //         op1: val2
  //       });
  //       return;
  //     } 
  //     if (calc.op1 !==0){
  //       if (calc.sign === "+") {
  //         setCalc({
  //           ...calc,
  //           res: val2 + calc.op1,
  //           op1: 0,
  //           sign: ""
  //         });
  //       }
  //     }


  //   }
  // }


  return (
    <Wrapper>
      <Screen value={calc.s < 2 ? calc.a : calc.r} />
      <ButtonBox>
        {btnValues.flat().map((btn, i) => {
          return (
            <Button
              key={i}
              className={btn === "=" ? "equals" : ""}
              value={btn}
              onClick={evaluator} />
          )
        })}
      </ButtonBox>
    </Wrapper>
  );
}




export default App;
